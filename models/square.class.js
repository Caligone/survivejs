function Square(xPos, yPos, type) {
	this.xPos = xPos;
	this.yPos = yPos;
	this.type = type;
}

Square.prototype.toString = function() {
	return "Square (" + this.xPos + " ; " + this.yPos + ") : " + this.type;
}
