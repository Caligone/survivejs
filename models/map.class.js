function Map(xSize, ySize) {
	this.xSize = xSize;
	this.ySize = ySize;
	this.squares = new Array(this.xSize);
	this.generate();
}

Map.prototype.toString = function() {
	var resultString ="";
	for(var x = 0 ; x < this.xSize ; x++) {
		for(var y = 0 ; y < this.ySize ; y++) {
			resultString += this.squares[x][y].type + " ";
		}
		resultString += "\n";
	}
	return resultString;
};

Map.prototype.generate = function() {
	var cptX, cptY, altX, altY;
	// Init the map
	for(cptX = 0 ; cptX < this.xSize ; cptX++) {
		this.squares[cptX] = new Array(this.ySize);
		for(cptY = 0 ; cptY < this.ySize ; cptY++) {
			this.squares[cptX][cptY] = new Square(cptX, cptY, TypeEnum.GRASS);
		}
	}

	// Moutains generations
	var nbMoutain = Math.floor((Math.random()*20)+1);
	for(var cptMoutain = 0 ; cptMoutain < nbMoutain ; cptMoutain++) {
		var mountainSizeHeight = Math.floor((Math.random()*8)+1);
		var mountainSizeWidth = Math.floor((Math.random()*8)+1);
		var mountainPosX = Math.floor((Math.random()*this.xSize));
		var mountainPosY = Math.floor((Math.random()*this.ySize));
		altX = 0;
		for(cptX = (mountainPosX-Math.floor(mountainSizeWidth/2) < 0 ? 0 : mountainPosX-Math.floor(mountainSizeWidth/2)) ; cptX < mountainPosX+Math.floor(mountainSizeWidth/2) && cptX < this.xSize ; cptX++) {
			for(cptY = (mountainPosY-Math.floor(mountainSizeHeight/2) < 0 ? 0 : mountainPosY-Math.floor(mountainSizeHeight/2)) ; cptY < mountainPosY+Math.floor(mountainSizeHeight/2) && cptY < this.ySize ; cptY++) {
				this.squares[cptX][cptY] = new Square(cptX, cptY, TypeEnum.MOUNTAIN);
			}
		}
	}

	// Water generations
	var nbWater = Math.floor((Math.random()*20)+1);
	for(var cptWater = 0 ; cptWater < nbWater ; cptWater++) {
		var waterSizeHeight = Math.floor((Math.random()*10)+1);
		var waterSizeWidth = Math.floor((Math.random()*10)+1);
		var waterPosX = Math.floor((Math.random()*this.xSize));
		var waterPosY = Math.floor((Math.random()*this.ySize));

		for(cptX = (waterPosX-Math.floor(waterSizeWidth/2) < 0 ? 0 : waterPosX-Math.floor(waterSizeWidth/2)) ; cptX < waterPosX+Math.floor(waterSizeWidth/2) && cptX < this.xSize ; cptX++) {
			for(cptY = (waterPosY-Math.floor(waterSizeHeight/2) < 0 ? 0 : waterPosY-Math.floor(waterSizeHeight/2)) ; cptY < waterPosY+Math.floor(waterSizeHeight/2) && cptY < this.ySize ; cptY++) {
				this.squares[cptX][cptY] = new Square(cptX, cptY, TypeEnum.WATER);
			}
		}
	}
};