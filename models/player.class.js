function Player() {
	this.x = 0;
	this.y = 0;
}

Player.prototype.move = function(xOffset, yOffset) {
	this.x += xOffset;
	this.y += yOffset;
};

Player.prototype.toString = function() {
	return "Le joueur se trouve sur la case ("+this.x+" ; "+this.y+")";
};