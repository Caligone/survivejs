var mapSize = 30;

var map = new Map(mapSize, mapSize);

var camera, controls, scene, renderer, projector, stats, objects, activeObject, lastColor;

init();
animate();

function init() {
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 );
	projector = new THREE.Projector();
	camera.position.z = mapSize;
	camera.lookAt(scene.position);
	controls = new THREE.OrbitControls( camera );
	controls.addEventListener( 'change', render );

	renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);

	objects = [];
	// Print the map
	for(var x = 0 ; x < map.squares.length ; x++) {
		for(var y = 0 ; y < map.squares[x].length ; y++) {
			var color = map.squares[x][y].type;
			if(color == TypeEnum.GRASS) {
				var geometry = new THREE.CubeGeometry(1,1,1);
				var material = new THREE.MeshLambertMaterial( { color: color, wireframe: false, shading: THREE.FlatShading  } );
				var cube = new THREE.Mesh(geometry, material);
				cube.position.x = map.squares[x][y].xPos-mapSize/2;
				cube.position.y = map.squares[x][y].yPos-mapSize/2;
				scene.add(cube);
				objects.push(cube);
			}
			else if(color == TypeEnum.MOUNTAIN) {
				var geometry = new THREE.CubeGeometry(1,1,1);
				var material = new THREE.MeshLambertMaterial( { color: color, wireframe: false, shading: THREE.FlatShading  } );
				var cube = new THREE.Mesh(geometry, material);
				cube.position.x = map.squares[x][y].xPos-mapSize/2;
				cube.position.y = map.squares[x][y].yPos-mapSize/2;
				scene.add(cube);
				objects.push(cube);
				var tetra = new THREE.CylinderGeometry(0, 0.7, 1, 4, 1)
				material = new THREE.MeshLambertMaterial( { color: color, wireframe: false, shading: THREE.FlatShading } );
				var mountain = new THREE.Mesh(tetra, material);
				mountain.position.x = map.squares[x][y].xPos-mapSize/2;
				mountain.position.y = map.squares[x][y].yPos-mapSize/2;
				mountain.position.z = 1;
				mountain.rotation.x = 1.56;
				mountain.rotation.y = 0.78;
				scene.add(mountain);
				objects.push(mountain);
			}
			else if(color == TypeEnum.WATER) {
				var geometry = new THREE.CubeGeometry(1,1,0.2);
				var material = new THREE.MeshLambertMaterial( { color: color, wireframe: false, shading: THREE.FlatShading  } );
				var cube = new THREE.Mesh(geometry, material);
				cube.position.x = map.squares[x][y].xPos-mapSize/2;
				cube.position.y = map.squares[x][y].yPos-mapSize/2;
				cube.position.z = -0.4;
				scene.add(cube);
				objects.push(cube);
			}
			
		}
	}

	light = new THREE.DirectionalLight( 0xffffff );
				light.position.set( 1, 1, 1 );
				scene.add( light );

				light = new THREE.DirectionalLight( 0x002288 );
				light.position.set( -1, -1, -1 );
				scene.add( light );

				light = new THREE.AmbientLight( 0x222222 );
				scene.add( light );


	stats = new Stats();
				stats.domElement.style.position = 'absolute';
				stats.domElement.style.top = '0px';
				stats.domElement.style.zIndex = 100;
				document.body.appendChild( stats.domElement );

	window.addEventListener( 'resize', onWindowResize, false );
	window.addEventListener( 'mousedown', onDocumentMouseDown, false );
}

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

	render();
}

function animate() {
	requestAnimationFrame(animate);
	controls.update();
}


function render() {
	renderer.render(scene, camera);
	stats.update();
}

function onDocumentMouseDown( event ) {
    event.preventDefault();
	var vector = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1, 0.5 );
	projector.unprojectVector( vector, camera );

	var raycaster = new THREE.Raycaster(camera.position, vector.sub( camera.position ).normalize() );

	var intersects = raycaster.intersectObjects(objects);

	if(intersects.length > 0 ){
		if(activeObject != undefined) {
			activeObject.object.material.color.setHex(lastColor);
		}
		lastColor = intersects[0].object.material.color.getHex();
		intersects[0].object.material.color.setHex(0xd0d0d0);
		activeObject = intersects[0];
	}
	render();
}

